#======================================================================================================================#
#
#                                            Project Title - Map of Research Projects
#
#======================================================================================================================#


# Load the libraries. I assume they are installed on the server.
library(bs4Dash)
library(shiny)
library(fresh)
library(leaflet)
library(dplyr)

#======================================================================================================================#
# Prepare the UI Theme ----
#======================================================================================================================#
bs4DashTheme <- create_theme(
  bs4dash_layout(main_bg = '#f8f9fa'),
  bs4dash_status(primary = '#3d9970', 
                 danger = '#BF616A', 
                 light = '#272c30'),
  bs4dash_sidebar_dark(bg = '#324559', 
                       color = '#c2c7d0',
                       hover_color = '#fff',
                       hover_bg = '#709f8a'),
  bs4dash_sidebar_light(bg = '#ffffff', 
                        color = '#000', 
                        hover_color = '#fff', 
                        hover_bg = '#709f8a'),
  bs4dash_font(weight_bold = 600, 
               family_base = 'sans-serif')
)


#======================================================================================================================#
#                                             UI Code ----
#======================================================================================================================#


ui <- bs4DashPage(
                  title = "Map",
                  header = bs4DashNavbar(),
                  sidebar = bs4DashSidebar(
                    expand_on_hover = FALSE,
                    disable = TRUE,
                    title = "Map"
                  ), dark = FALSE,
                  
                  
                  body = bs4DashBody(
                    use_theme(bs4DashTheme),
                    fluidRow(
                      box(title = 'Map', 
                          width = 8,
                          solidHeader = TRUE,
                          status = 'primary', 
                          collapsible = FALSE, 
                          maximizable = TRUE, 
                          sidebar = boxSidebar(id = 'map_sidebar',
                                               startOpen = FALSE,
                                               width = 30,
                                               selectInput(inputId = 'map_type', 
                                                           label = 'Map Type',
                                                           choices = names(leaflet::providers), 
                                                           multiple = FALSE,
                                                           selected = 'CartoDB.Positron'),
                                               selectInput(inputId = 'selected_country', 
                                                           label = 'Select Country', 
                                                           choices = 'placeholder'),
                                               selectInput(inputId = 'selected_typeof_study', 
                                                           label = 'Select Type Study', 
                                                           choices = 'placeholder'),
                                               selectInput(inputId = 'selected_discipline', 
                                                           label = 'Select Discipline', 
                                                           choices = 'placeholder')
                                               
                                               ),
                          shinycssloaders::withSpinner(
                       leafletOutput(outputId = 'leaflet_map', height = 560), type = 8),
                       box(title = 'Static Map', 
                           maximizable = TRUE, 
                           collapsible = TRUE, 
                           status = 'primary',
                           solidHeader = TRUE,
                           width = 12,
                           plotOutput(outputId = 'static_map', height = 950))
                       ),
                    box(title = 'Raw Data', 
                        width = 4,
                        solidHeader = TRUE,
                        status = 'primary', 
                        collapsible = FALSE, 
                        maximizable = TRUE,
                       DT::dataTableOutput(outputId = 'input_tbl'))
                    )
                        
                        
                      
                    
                  )
)



#======================================================================================================================#
#                                             Server Code ----
#======================================================================================================================#
server <- function(input, output, session) {
  
  # Load the data set ----
  # screen_light         <- readr::read_csv("TW MA - Screening_light.csv")
  # centroid_coordinates <- readr::read_csv("centroid_coordinates.csv")
  # screen_light         <- screen_light %>% left_join(centroid_coordinates, by = c('Country' = 'Country'))
  # screen_light$Lat     <- screen_light$Lat + rnorm(n = nrow(screen_light), mean = 0, sd = 1)
  # screen_light$Lon     <- screen_light$Lon + rnorm(n = nrow(screen_light), mean = 0, sd = 1)
  # screen_light$Study   <- paste0(screen_light$Year, ', ', screen_light$Authors)
  # 
  screen_light <- readxl::read_excel("screen_light.xls")
  
  observe({
    
  updateSelectInput(session, inputId = 'selected_country', 
                    choices = c('All', unique(screen_light$Country)),
                    selected = 'All')  
  updateSelectInput(session, inputId = 'selected_typeof_study', 
                    choices = c('All', unique(screen_light$`Type of study`)),
                    selected = 'All')  
  updateSelectInput(session, inputId = 'selected_discipline', 
                    choices = c('All', unique(screen_light$`Journal discipline`)), 
                    selected = 'All')  
    
  })
  
  
  #====================================================================================================================#
  #                                              Leaflet Map ----
  #====================================================================================================================#
  
  # Prepare the data
  map_data <- reactive({
    
    raw_data <- screen_light
    if(input$selected_country != 'All'){
      raw_data <- screen_light[screen_light$Country == input$selected_country, ]
    }
    
    if(input$selected_typeof_study != 'All'){
      raw_data <- raw_data[raw_data$`Type of study` == input$selected_typeof_study, ]
    }
    
    if(input$selected_discipline != 'All'){
      raw_data <- raw_data[raw_data$`Journal discipline` == input$selected_discipline, ]
    }
    
    return(raw_data)
    
  })
  
  
  # Render the map
  output$leaflet_map <- leaflet::renderLeaflet({
      
    map_data <- map_data()
    
    ## Use C formatting for the data displayed on mouse hover
      labels <- sprintf(
        "<strong> Year:</strong> %s<br/>
         <strong> Authors:</strong> %s<br/> 
         <strong>Journal Discipline:</strong> %s <br/> 
         <strong>Type of Study</strong>: %s <br/> 
         <strong>Country</strong>: %s <br/>", 
        map_data$Year,
        map_data$Authors,
        map_data$`Journal discipline`,
        map_data$`Type of study`,
        map_data$Country
        ) %>% 
        lapply(HTML)
      
      pal <- colorFactor(
        palette = c('brown', 'blue', 'gray'),
        domain = map_data$`Type of study`
      )
      
      cf       <- colorFactor( c('brown', 'blue', 'gray'), map_data$`Type of study`) 
      map_type <- input$map_type
      
      map <- tryCatch({leaflet(map_data) %>%
            addProviderTiles(provider = map_type, 
                             options = providerTileOptions(minZoom = 1, maxZoom = 11, zIndex = 2))  %>%
            addCircleMarkers(~Lon, ~Lat, 
                             label = ~labels, 
                             color = ~ pal(`Type of study`),
                             radius = 14,
                             labelOptions = labelOptions(noHide = FALSE, direction = "bottom",
                                                         style = list(
                                                           "color" = "black",
                                                           "font-family" = "serif",
                                                           "font-style" = "italic",
                                                           "box-shadow" = "3px 3px rgba(0,0,0,0.25)",
                                                           "font-size" = "14px",
                                                           "border-color" = "rgba(0,0,0,0.5)"
                                                         ))
            ) %>% addLegend("bottomright",
                      pal = cf,
                      values =  map_data$`Type of study`,
                      title = "Type of Study",
                      opacity = 1
            )}, error = function(e){'Error'})
      
      
      validate(
        need(map!='Error', message = 'No data available for the selected options.')
        )
      return(map)
    
  })
  
  
  
  output$static_map <- renderPlot({
    map_data              <- map_data()
    map_data$index        <- 1:nrow(map_data)
    map_data$legend_label <- factor(paste0(map_data$index, '. ', map_data$Study))
    template              <- readRDS('map_tiles_data.rds')
    
    require(ggmap)
    ggmap(template) + 
      geom_point(data = map_data,
                 aes(x = Lon, y = Lat, fill = legend_label), size = 3)+
      geom_label(data = map_data, aes(x = Lon, y = Lat, label = index), size = 3) +
      theme(legend.position = 'bottom',
            legend.background = element_rect(colour = 'black', fill = 'grey90', size = 1, linetype='solid'), 
            axis.line = element_line(colour = "black", 
                                     size = 1, 
                                     linetype = "solid"),
            axis.ticks.length.x = unit(0.15, 'cm'), 
            text = element_text(family = 'sans-serif'))+
      xlab('Latitude')+
      ylab('Longitude')
    
    
    
  })
  
  
  # Render the data table
  output$input_tbl <- DT::renderDataTable({
    req(map_data())
    
    map_data() %>% 
      mutate_if(is.numeric, round, 2)%>%
    DT::datatable(caption = '',
                  rownames = FALSE,
                  style = 'bootstrap',
                  class = 'cell-border stripe',
                  extensions = c("Buttons", 'Scroller'),
                  options = list(dom = 'Bfrtip', 
                                 scrollY = 420,
                                 scroller = TRUE,
                                 scrollX = TRUE,
                                 buttons = list('copy', 'print', 
                                                list(extend = 'collection',
                                                     buttons = c('csv', 'excel', 'pdf'),
                                                     text = 'Download Data')
                                 )
                  ), 
                  escape = FALSE
                  
                  )
    
  })
  
}


#======================================================================================================================#
#                                              Run the application ----
#======================================================================================================================#
shinyApp(ui, server)